from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json



class LocationDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
        "id",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric_type",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationDetailEncoder(),
    }



@require_http_methods(["GET", "POST", "PUT"])
def api_list_hats(request, pk=id):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            print(location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Hat"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "POST", "PUT"])
def api_get_hat(request, pk=id):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    elif request.method == "DELETE":
        try:
            hats = Hat.objects.get(id=pk)
            hats.delete()
            return JsonResponse(
                hats,
                encoder=HatListEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)

            props = ["fabric_type", "style_name", "color", "picture_url", "location"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatListEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
