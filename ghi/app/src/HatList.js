import Nav from './Nav'
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatList() {

  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const deleteHat = async (id) => {
    const response = await fetch(`http://localhost:8090/api/hats/${id}/`, {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    console.log("hat deleted")
    setHats(
      hats.filter((hat) => {
        return hat.id !== id;
      })
    )
  }

  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>URL</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hat) => {
            return (
              <tr key={hat.id}>
                <td>{hat.fabric_type}</td>
                <td>{hat.style_name}</td>
                <td>{hat.color}</td>
                <td>{hat.picture_url}</td>
                <td>{hat.location.closet_name}</td>
                <td>
                  <button onClick={() => deleteHat(hat.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Create a Hat</Link>
      </div>
    </>
  );
}

export default HatList;
