import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* <Route path="/shoes" element={<ShoesList />} />
          <Route path="shoes">
            <Route path="new" element={<ShoesForm />} />
          </Route> */}
          <Route path="/hats" element={<HatList />} />
          <Route path="hats">
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
        {/* <Routes>
          <Route path="/shoes" element={<ShoesList />} />
        </Routes> */}
        {/* <Routes>
          <Route path="/shoes/create" element={<ShoesForm />} />
        </Routes> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
