# Wardrobify

Team:

* Person 2 - Bradley Daniels - Shoes
* Person 1 - Braden - Hats

## Design

## Shoes microservice

The shoes microservice will make a call to the wardrobe.

## Hats microservice
The hats microservice will make calls to the wardrobe to see which hats are in there.

Explain your models and integration with the wardrobe
microservice, here.
